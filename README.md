# CodeChallenge

***CodeChallenge, Said Shamkhali.***

## First steps

For Ansbile to work, first you need to download it:
```
sudo yum install ansible
sudo yum install python-pip
sudo pip install boto
```
After this, do the following:
* Create IAM user and save credentials.
* Create .boto file and paste the next code (the key information its in the saved credentials):
```
[Credentials]
aws_access_key_id = <your_access_key_here>
aws_secret_access_key = <your_secret_key_here>
```
also you can:
```
export AWS_ACCESS_KEY_ID=<your_access_key_here>
export AWS_SECRET_ACCESS_KEY=<your_secret_key_here>
```
* Create keypair, in this case CodeChallenge.

Finally execute the .yml file containing ansible code. The code can be found in the Ansible directory in this repository:
```
ansible-playbook task.yml
```

## Swagger import

Go to Swagger directory where swagger-users-v1.json (Swagger template) is located and execute the following code to deploy the server at API Gateway (remember to export credentials and set the permissions to perform this):
```
aws apigateway import-rest-api --body file://swagger-users-v1.json --region eu-west-1
```
If the import was succesfully done, the following should show at API Gateway:
![Alt text](./img/Swagger1.PNG)

Users (API Gateway):
![Alt text](./img/Swagger2.PNG)

## Auto scaling and monitoring

* Attach the ec2 instance to an Auto scaling Group:
![Alt text](./img/AutoScaling1.PNG)

* Configurate the group so it can have more than 1 instance (max instances):
![Alt text](./img/AutoScaling2.PNG)

* Create an alarm so, when CPU utilization (average) is equal or more than 25%, a second instance is created:
![Alt text](./img/AutoScaling3.PNG)

## Testing and Postman

This section is being worked on. Unfortunately, there is no Postman configuration or Jmeter loading test (.jmx file).